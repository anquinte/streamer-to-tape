# -*- coding: utf-8 -*-
"""
File DAO. Represents a file data access object.
Author: Juan Pablo Vinchira Salazar
Contact: cms-tier0-operations@cern.ch
"""

# Utils
from utils.database import Database

# Models
from models.file import File

class FileDAO:

    def __init__(self):
        """
        Creates a database connection for the new RunDao instance
        """
        self.db = Database()
        self.db.connect()

    def __del__(self):
        """
        Closes the database connection when the object is deleted
        """
        self.db.conn.close()

    def insert_file(self, file):
        """
        Inserts a new file on the database.

        Arguments:
            file (File): New File instance.
        """

        # Sentence to insert a new file
        sql = '''
            INSERT INTO file (
                path, adler32, filesize,
                archived, staged, run_id)
            VALUES (?, ?, ?, ?, ?, ?);
        '''

        # Inserts the new file
        self.db.conn.execute(sql,
            (file.path, file.adler32,
            file.filesize, file.archived, file.staged, file.run_id))
        self.db.conn.commit

    def bulk_files(self, run_id, files):
        """
        Inserts massively a list of files on the database

        Arguments:
            run_id (Integer): 6 digits integer representing a T0 run.
            files (List): List of tuples with files information (path, adler32, filesize, archived, staged, run_id)
        """

        # Sentence to insert a new file
        sql = '''
            INSERT INTO file (
                path, adler32, filesize,
                archived, staged, run_id)
            VALUES (?, ?, ?, ?, ?, ?);
        '''

        # Inserts the list of specified files
        self.db.conn.executemany(sql, files)
        self.db.conn.commit()

    def get_without_adler32(self):
        """
        Retrieves a list of files that doesn't have adler32 sum.

        Return:
            files (List): List of File instances that doesn't have adler32 sum.
        """

        # Sentence to retrieve files withouth adler32 sum
        sql = '''
           SELECT rowid, path, adler32, filesize, archived, staged, run_id
           FROM file WHERE adler32 IS '';
        '''

        # Empty list to store File objects without adler32 sum
        files = []

        # For each File without adler32 sum
        for each_file in self.db.conn.execute(sql):

            # Creates a new File instance for the current file
            file = File()
            file.file_id = each_file[0]
            file.path = each_file[1]
            file.adler32 = each_file[2]
            file.filesize = each_file[3]
            file.archived = each_file[4]
            file.staged = each_file[5]
            file.run_id = each_file[6]

            # Adds the new File to the files list
            files.append(file)

        # Returns the files list
        return files

    def get_not_archived(self, run_id):
        """
        Retrieves a list of not archived files for an specific run.

        Arguments:
            run_id (Integer): 6 digits integer representing a T0 run.

        Return:
            files (List): List of non archived File instances of an specific run.
        """

        # Sentence to retrieve non archived files for an specific run
        sql = '''
            SELECT rowid, path, adler32, filesize, archived, staged, run_id
            FROM file
            WHERE archived == 0
            AND run_id == ?;
        '''

        # Empty list to store non archived files
        files = []

        # For each non archived file of the specified run
        for each_file in self.db.conn.execute(sql,(run_id,)):

            # Creates a new File instance for the current file
            file = File()
            file.file_id = each_file[0]
            file.path = each_file[1]
            file.adler32 = each_file[2]
            file.filesize = each_file[3]
            file.archived = each_file[4]
            file.staged = each_file[5]
            file.run_id = each_file[6]

            # Adds the new File to the non archived files list
            files.append(file)

        # Returns the non archived files list
        return files

    def get_not_staged(self, run_id):
        """
        Retrieves a list of non staged files for an specific run.

        Arguments:
            run_id (Integer): 6 digits integer representing a T0 run.

        Return:
            files (List): List of non staged File instances of an specific run.
        """

        # Sentence to retrieve non staged files for an specific run
        sql = '''
            SELECT rowid, path, adler32, filesize, archived, staged, run_id
            FROM file
            WHERE staged == 0
            AND run_id == ?;
        '''

        # Empty list to store non staged files
        files = []

        # For each non staged file of the specified run
        for each_file in self.db.conn.execute(sql,(run_id,)):

            # Creates a new File instance for the current file
            file = File()
            file.file_id = each_file[0]
            file.path = each_file[1]
            file.adler32 = each_file[2]
            file.filesize = each_file[3]
            file.archived = each_file[4]
            file.staged = each_file[5]
            file.run_id = each_file[6]

            # Adds the new File to the non staged files list
            files.append(file)

        # Returns the non staged files list
        return files

    def update_adler32(self, files):
        """
        Updates the adler32 sum for the specified files.

        Arguments:
            files (List): List of tuples (adler32, rowid), where rowid is the default SQLite primary key.
        """

        # Sentence to update the file's adler32 sum
        sql = '''
            UPDATE file SET adler32 = ?
            WHERE rowid = ?;
        '''

        # Updates the adler32 sum of the specified runs
        self.db.conn.executemany(sql,files)
        self.db.conn.commit()

    def set_archived_inv(self, run_id, failed):
        """
        Sets the specified files as non archived.

        Arguments:
            run_id (Integer): 6 digits integer representing a T0 run.
            failed (List): List of paths of files to be set as non archived.
        """

        # Sentence to set all the files of an specific run as archived
        sql = '''
            UPDATE file SET archived = 1
            WHERE run_id = ? and archived = 0;
        '''

        # Set all the files of an specific run as archived
        self.db.conn.execute(sql, (run_id,))

        # Sentence to set the specified file (by path) as non archived
        sql = '''
            UPDATE file SET archived = 0
            WHERE path = ?;
        '''

        # for each file in the failed files list
        nfile = 0
        while nfile < len(failed):
            # Converts the String in a Tuple with an String
            failed[nfile] = (failed[nfile],)
            nfile += 1

        # Sets the specified files as non archived
        self.db.conn.executemany(sql,failed)
        self.db.conn.commit()

    def set_staged_inv(self, run_id, failed):
        """
        Sets the specified files as non staged.

        Arguments:
            run_id (Integer): 6 digits integer representing a T0 run.
            failed (List): List of paths of files to be set as non staged.
        """

        # Sentence to set all the files of an specific run as staged
        sql = '''
            UPDATE file SET staged = 1
            WHERE run_id = ? and staged = 0;
        '''

        # Set all the files of an specific run as staged
        self.db.conn.execute(sql, (run_id,))

        # Sentence to set the specified file (by path) as non staged
        sql = '''
            UPDATE file SET staged = 0
            WHERE path = ?;
        '''

        # for each file in the failed files list
        nfile = 0
        while nfile < len(failed):
            # Converts the String in a Tuple with an String
            failed[nfile] = (failed[nfile],)
            nfile += 1

        # Sets the specified files as non staged
        self.db.conn.executemany(sql,failed)
        self.db.conn.commit()

    def get_run_files(self, run_id):
        """
        Gets the files of an specific run

        Arguments:
            run_id (Integer): 6 digits integer representing a T0 run.

        Return:
            files (List): List of files
        """

        # Sentence to retrieve the files of an specific run
        sql = '''
            SELECT rowid, path, adler32, filesize, archived, staged, run_id
            FROM file
            WHERE run_id = ?;
        '''

        # List to store the retrieved files
        files = []

        # For each retrieved file
        for each_file in self.db.conn.execute(sql,(run_id,)):

            # Creates a new File instance for the current file
            file = File()
            file.file_id = each_file[0]
            file.path = each_file[1]
            file.adler32 = each_file[2]
            file.filesize = each_file[3]
            file.archived = each_file[4]
            file.staged = each_file[5]
            file.run_id = each_file[6]

            # Adds the new File to the files list
            files.append(file)

        # Returns the list of files
        return files
