# -*- coding: utf-8 -*-
"""
Job DAO. Represents a transfer job data access object.
Author: Juan Pablo Vinchira Salazar
Contact: cms-tier0-operations@cern.ch
"""

# Utils
from utils.database import Database

# Models
from models.job import Job

class JobDAO:

    def __init__(self):
        """
        Creates a database connection for the new RunDao instance
        """
        self.db = Database()
        self.db.connect()

    def __del__(self):
        """
        Closes the database connection when the object is deleted
        """
        self.db.conn.close()

    def insert_job(self, job):
        """
        Inserts a new job on the database.

        Arguments:
            job (Job): Job object.
        """

        # Sentence to insert a new job
        sql = '''
            INSERT INTO job (
                job_id, job_type, job_state,
                start_date, end_date, run_id,
                submit_file, active)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?);
        '''

        # Inserts a new job on the database
        self.db.conn.execute(sql,
            (job.job_id, Job.JOB_TYPE[job.job_type],
            Job.JOB_STATE[job.job_state], job.start_date,
            job.end_date, job.run_id, job.submit_file,
            job.active))
        self.db.conn.commit()

    def update_states(self, states):
        """
        Updates the state of the specified jobs

        Arguments:
            states (List): List of Tuples with the (job_state, end_date, job_id) to update.
        """

        # Sentence to update a job_state and end_date
        sql = '''
            UPDATE job SET job_state = ?, end_date = ?
            WHERE job_id = ?;
        '''

        # Updates the job_state and end_date of the specified jobs
        self.db.conn.executemany(sql,states)
        self.db.conn.commit()

    def get_running_jobs(self):
        """
        Gets the jobs with state "SUBMITTED" or "ACTIVE".

        Return:
            jobs (List): List of Job instances with the active jobs.
        """

        # Sentence to retrieve the jobs with state "SUBMITTED" or "ACTIVE"
        sql = '''
            SELECT j.job_id, jt.type, js.state,
            j.start_date, j.end_date, j.run_id, j.submit_file, j.active
            FROM job as j INNER JOIN job_state AS js ON j.job_state = js.state_id
            INNER JOIN job_type AS jt ON j.job_type = jt.type_id
            WHERE job_state = {0} or job_state = {1};
        '''.format(Job.JOB_STATE['SUBMITTED'],Job.JOB_STATE['ACTIVE'])

        # Empty list to store running jobs
        jobs = []

        # For each running job
        for each_job in self.db.conn.execute(sql):

            # Creates a new Job instance for the current job
            job = Job()
            job.job_id = each_job[0]
            job.job_type = each_job[1]
            job.job_state = each_job[2]
            job.start_date = each_job[3]
            job.end_date = each_job[4]
            job.run_id = each_job[5]
            job.submit_file = each_job[6]
            job.active = each_job[7]

            # Adds the new job to the jobs list
            jobs.append(job)

        # Returns the jobs list
        return jobs

    def get_active_job(self, run_id):
        """
        Gets the currently active job of an specific run.

        Arguments:
            run_id (Integer): 6 digits integer representing a T0 run.

        Return:
            job (Job): Job instance with the currently active job of the specified run. If there's no active job, then None is returned.
        """

        # Sentence to retrieve the active jobs of an specific run
        sql = '''
            SELECT j.job_id, jt.type, js.state,
            j.start_date, j.end_date, j.run_id, j.submit_file, j.active
            FROM job as j INNER JOIN job_state AS js ON j.job_state = js.state_id
            INNER JOIN job_type AS jt ON j.job_type = jt.type_id
            WHERE run_id = ? AND active = 1;
        '''

        # Retrieves only one active job for the specified run (there must be only one)
        cur = self.db.conn.execute(sql, (run_id,))
        active_job = cur.fetchone()

        # If there's an active job
        if active_job:

            # Creates a new instance of Job for the retrieved job
            job = Job()
            job.job_id = active_job[0]
            job.job_type = active_job[1]
            job.job_state = active_job[2]
            job.start_date = active_job[3]
            job.end_date = active_job[4]
            job.run_id = active_job[5]
            job.submit_file = active_job[6]
            job.active = active_job[7]

            # Returns the active job
            return job
        else:
            return None

    def deactivate_job(self, job_id):
        """
        Deactivates a job

        Arguments:
            job_id (String): UUID of the job.
        """

        # Sentence to deactivate a job based on its UUID
        sql = '''
            UPDATE job SET active = 0 WHERE job_id = ?;
        '''

        # Deactivates the specified job
        self.db.conn.execute(sql, (job_id,))
        self.db.conn.commit()
