# -*- coding: utf-8 -*-
"""
Manages a database connection
Author: Juan Pablo Vinchira Salazar
Contact: cms-tier0-operations@cern.ch
"""

# Utils
from utils.config import Config

# Database libs
import sqlite3

class Database:

    def __init__(self):
        """
        Loads the database configuration from the configuration file.
        Currently only SQLite is supported.
        """
        config = Config()
        self.url = config.files['database']

    def connect(self):
        """
        Creates a database connection.
        If it's an SQLite connection and the database doesn't exist, then a new one is created.
        """
        self.conn = sqlite3.connect(self.url)

    def close(self):
        """
        Close the current database connection.
        """
        self.conn.close()
