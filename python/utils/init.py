# -*- coding: utf-8 -*-
"""
Initialize an SQLite database
Author: Juan Pablo Vinchira Salazar
Contact: cms-tier0-operations@cern.ch
"""

# Utils
from utils.database import Database

# Models
from models.job import Job

def init_database():
    """
    Create the database structure and constraints on an empty SQLite database
    """

    # Connects to the database
    db = Database()
    db.connect()
    c = db.conn.cursor()

    # Create tables sentences
    c.executescript('''
        CREATE TABLE run (
            run_id INT NOT NULL,
            archived INT,
            staged INT,
            PRIMARY KEY (run_id)
        );

        CREATE TABLE job_type (
            type_id INT NOT NULL,
            type TEXT,
            PRIMARY KEY (type_id)
        );

        CREATE TABLE job_state (
            state_id INT NOT NULL,
            state TEXT,
            PRIMARY KEY (state_id)
        );

        CREATE TABLE job (
            job_id TEXT NOT NULL,
            job_type INT NOT NULL,
            job_state INT NOT NULL,
            start_date DATETIME,
            end_date DATETIME,
            run_id INT NOT NULL,
            submit_file TEXT,
            active INT,
            PRIMARY KEY (job_id),
            FOREIGN KEY (run_id) REFERENCES run (run_id),
            FOREIGN KEY (job_type) REFERENCES job_type (type_id),
            FOREIGN KEY (job_state) REFERENCES job_state (state_id)
        );

        CREATE TABLE file (
            path TEXT NOT NULL,
            adler32 TEXT NOT NULL,
            filesize INT NOT NULL,
            archived INT,
            staged INT,
            run_id INT,
            FOREIGN KEY (run_id) REFERENCES run (run_id),
            UNIQUE (path)
        );
    ''')

    # Creates an empty list to store tuples with the Job types stored in the job model
    new_rows = []

    # For each Job type in the job model
    for each_key in Job.JOB_TYPE.keys():

        # Add the current tuple (job_type_id, job_type) to the job types list
        new_rows.append((Job.JOB_TYPE[each_key], each_key))

    # Insert sentence to insert the job types in the database
    sql = '''
        INSERT INTO job_type (type_id, type) VALUES (?, ?)
    '''

    # Insert the job types in the database
    db.conn.executemany(sql,new_rows)
    db.conn.commit()

    # Creates an empty list to store tubles with the Job states stored in the job model
    new_rows = []

    # For each Job state in the job model
    for each_key in Job.JOB_STATE.keys():

        # Add the current tuple (job_state_id, job_state) to the job states list
        new_rows.append((Job.JOB_STATE[each_key], each_key))

    # Insert sentence to insert the job states in the database
    sql = '''
        INSERT INTO job_state (state_id, state) VALUES (?, ?)
    '''

    # Insert the job states in the database
    db.conn.executemany(sql,new_rows)
    db.conn.commit()

    # Close the current database connection
    db.close()
