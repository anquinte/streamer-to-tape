# -*- coding: utf-8 -*-
"""
Streamer files to tape main script
This script defines the main workflow to archive/retrieve files from/to tape
Author: Juan Pablo Vinchira Salazar
Contact: cms-tier0-operations@cern.ch
"""

# Controllers
from controllers.manage_runs import ManageRuns
from controllers.manage_jobs import ManageJobs
from controllers.manage_files import ManageFiles

# Utils
from utils.config import Config
import utils.init as init

# Python std libs
import json
import os

# Loads configuration
config = Config()

# Loads the current VOMS Proxy path as an environment variable
os.environ['X509_USER_PROXY'] = config.files['voms_proxy']

# Dictionary with the list of runs to process
runs = {}

# Loads runs list file
with open(config.files['runs'], 'r') as ifile:

    # Loads the runs on the configuration file in the runs dictionary
    runs = json.load(ifile)['runs']

# Checks if the database file exists
if not os.path.isfile(config.files['database']):

    # Creates a new database file
    init.init_database()

# Updates the state of the current jobs
mj = ManageJobs()
mj.update_states()

# Looks for and tries to correct inconsistencies on the files information stored in the database
mf = ManageFiles()
mf.check_files_info()

# Creates a new ManageRuns instance to process the specified runs
mg = ManageRuns()

# Creates a list to store the information of the submitted runs
current_runs = []

# For each run in the runs dictionary
for each_run in runs:

    # Attempts to submit the current run to FTS3
    run = mg.submit_run(each_run['id'], each_run['type'])

    # If a new run object is returned (run submitted successfully)
    if run:
        # Adds the run with the new information to the runs list
        current_runs.append(run)
    else:
        # Keeps the run information as is (is not deleted to allow the user corrects it)
        current_runs.append(each_run)

# Creates a new JSON file to store the updated information of the runs submitted to FTS3
with open(config.files['runs'], 'w') as ofile:

    # Creates the JSON file using the list with the current runs
    json.dump({'runs':current_runs}, ofile, indent=4)
