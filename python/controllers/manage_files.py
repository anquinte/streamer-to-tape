# -*- coding: utf-8 -*-
"""
Files manager.
Author: Juan Pablo Vinchira Salazar
Contact: cms-tier0-operations@cern.ch
"""

import subprocess

# Utils
from utils.config import Config

# DAO
from dao.file import FileDAO

class ManageFiles:

    def __init__(self):
        """
        Initialize the class global variables using the configuration file.

        Attributes:
            fts3 (String): Path of the FTS3 script wrapper.
            source_path (String): Path where the streamer files are hosted.
        """

        config = Config()
        self.fts3 = config.scripts['fts3']
        self.source_path = config.source['path']

    def check_files_info(self):
        """
        Looks for inconsistencies on the database and tries to correct them.
        """

        print('Checking files information...')

        retries = 0

        while retries < 4:
            with_errors = False

            # Checks if all the files has adler32 sums
            with_errors = self.check_adler32()

            if with_errors:
                retries += 1
            else:
                return

        print('There are errors in the files information. Check the logs for more information.')

    def check_adler32(self):
        """
        Looks for files without adler32 sums and tries to update them.
        """

        with_errors = False

        print('Retrieving files without adler32 sum...')

        # Retrieves the files without adler32 sum
        file_dao = FileDAO()
        files = file_dao.get_without_adler32()

        # If a file with error is spotted
        if len(files) > 0:
            with_errors = True

        # Empty list to store tuples (adler32, file_id)
        adler32_sums = []

        # For each file without adler32 sum
        for each_file in files:

            print('Retrieving adler32 sum of file "{0}"'.format(each_file.path))

            # Retrieves the adler32 sum of the current file
            file_path = '{0}/{1}'.format(self.source_path, each_file.path)
            cmd = subprocess.Popen([self.fts3, 'get_adler32', file_path],
                                    stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            adler32, stderr = cmd.communicate()
            adler32 = adler32.strip()

            # If there're no errors and the adler32 sum is not empty
            if len(stderr) == 0 and len(adler32) > 0:

                # Adds the tuple (adler32, file_id) to the list of sums to update
                adler32_sums.append((adler32, each_file.file_id))

            else:
                print('Error obtaining the adler32 sum for the file "{0}". {1}'.format(file_path, stderr))

        # If there're sums to update
        if len(adler32_sums) > 0:

            # Updates the specified adler32 sums
            print('Updating empty adler32 sums')
            file_dao.update_adler32(adler32_sums)
            print('Done')

        # Returns True if there's any error
        return with_errors
