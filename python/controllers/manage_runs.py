# -*- coding: utf-8 -*-
"""
Runs manager.
Author: Juan Pablo Vinchira Salazar
Contact: cms-tier0-operations@cern.ch
"""

from tempfile import TemporaryFile
from datetime import datetime
import subprocess
import json
import time
import os

# Utils
from utils.config import Config

# Models
from models.run import Run
from models.file import File
from models.job import Job

# DAO
from dao.run import RunDAO
from dao.file import FileDAO
from dao.job import JobDAO

class ManageRuns:

    def __init__(self):
        """
        Initialize the class global variables using the configuration file.

        Attributes:
            fts3 (String): Path of the FTS3 script wrapper.
            stager (String): Path of the CASTOR stager script wrapper.
            source_server (String): Server where the source files are stored.
            source_srv_params (String): Additional parameters used by the server appended to the URL.
            source_prefix (String): Path where the streamer files are hosted.
            destination_server (String): Server where the files will be sent.
            destination_prefix (String): Path where the streamer files will be sent.
            destination_srv_params (String): Additional parameters used by the server appended to the URL.
            data_path (String): Relative path to read/write the streamer files (it's the same on source and destination).
            json_path (String): Path of the JSON file with the run to be transferred.
            source (String): source_prefix/data_path.
            destination (String): destination_prefix/data_path.
        """

        config = Config()
        self.fts3 = config.scripts['fts3']
        self.stager = config.scripts['stager']
        self.source_server, self.source_srv_params = config.source['server'].split('?', 1)
        self.source_prefix = config.source['path']
        self.destination_server, self.destination_srv_params = config.destination['server'].split('?', 1)
        self.destination_prefix = config.destination['path']
        self.data_path = config.paths['data']
        self.json_path = config.paths['json']
        self.source = '{0}/{1}'.format(self.source_prefix, self.data_path)
        self.destination = '{0}/{1}'.format(self.destination_prefix, self.data_path)

    def save_new_run(self, run_id):
        """
        Saves the information of a run on the database.

        Arguments:
            run_id (Integer): 6 digits integer representing a T0 run.
        """

        # Temporary files to keep the output of FTS3 wrapper
        stdout = TemporaryFile()
        stderr = TemporaryFile()

        # Retrieve the list of run's files
        cmd = subprocess.Popen([self.fts3, 'get_run_files_csv', self.source, run_id],
            stdout=stdout)
        cmd.wait()
        stdout.seek(0)

        # DAO instances
        run_dao = RunDAO()
        file_dao = FileDAO()

        # Creates a new run instance
        run = Run()
        run.run_id = run_id

        # Inserts the new run on the database
        run_dao.insert_run(run)

        # Empty list to store the run files
        files = []

        # For each run file
        for each_line in stdout:
            # Splits the FTS3 output
            filesize, path, adler32 = each_line.strip().split(',',2)

            # Adds a Tuple (path, adler32, filesize, archived, staged, run_id) to the files list
            files.append((path.replace(self.source_prefix,'',1), adler32, filesize, False, True, run_id))

        # Saves all the files to the datase
        file_dao.bulk_files(run_id, files)

        # Close temporary files
        stdout.close()
        stderr.close()

    def submit_run(self, run_id, job_type):
        """
        Submits a transfer job.

        Arguments:
            run_id (Integer): 6 digits integer representing a T0 run.
            job_type (String): Type of the job. The job types are defined in Job model.
        """

        # Retrieves the run from the database
        run_dao = RunDAO()
        run = run_dao.get_run(run_id)

        # If the run doesn't exists
        if not run:
            # Save the new run
            print('Processing run {0}'.format(run_id))
            self.save_new_run(run_id)
            run = run_dao.get_run(run_id)

        # If the run exists
        if run:

            # Retrieves the run's active job
            job_dao = JobDAO()
            active_job = job_dao.get_active_job(run_id)

            # If there's an active job
            if active_job:

                # If the active job is complete in a dirty way
                if (active_job.job_state == 'FINISHEDDIRTY'
                    or active_job.job_state == 'FAILED'
                    or active_job.job_state == 'CANCELED'):

                    # Deactivates the job
                    print('Deactivating job {0}'.format(active_job.job_id))
                    job_dao.deactivate_job(active_job.job_id)

                # If the active job is complete without errors
                elif active_job.job_state == 'FINISHED':

                    # If it's an archiving job
                    if active_job.job_type == 'archive':
                        # Set the run as archived
                        print('Updating run {0} to archived'.format(run_id))
                        run_dao.set_archived(run_id)

                    # If it's an retrieving job
                    elif active_job.job_type == 'retrieve':
                        # Set the run as staged
                        print('Updating run {0} to staged'.format(run_id))
                        run_dao.set_staged(run_id)

                    # Deactivate the completed job
                    print('Deactivating job {0}'.format(active_job.job_id))
                    job_dao.deactivate_job(active_job.job_id)

                    # Return the job information with the new state
                    return {
                            'id':run_id,
                            'type':job_type,
                            'job_id':active_job.job_id,
                            'state':active_job.job_state
                           }

                else:

                    # Return the job information with the new state
                    return {
                            'id':run_id,
                            'type':job_type,
                            'job_id':active_job.job_id,
                            'state':active_job.job_state
                           }

            # Checks if the job is already done before attempts to start it
            if (((job_type == 'archive') and (not run.archived))
                or
                ((job_type == 'retrieve') and (not run.staged))):

                # If it's a retrieve request, checks if all the files are prestaged
                if job_type == 'retrieve':

                    print('Checking pre-stage status of run {0}...'.format(run_id))

                    # Control variable that's False if the run is not entirely staged
                    prestaged = True

                    # Gets the list of the files of the run
                    files_dao = FileDAO()
                    files = files_dao.get_run_files(run.run_id)

                    # Checks the prestage status of each file
                    for each_file in files:

                        # Retrieves the current file stage state
                        file_path = '{0}/{1}'.format(self.destination_prefix, each_file.path)
                        cmd = subprocess.Popen([self.stager, 'get_state', file_path],
                                              stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                        state, stderr = cmd.communicate()
                        state = state.strip()
                        stderr = stderr.strip()

                        # If there's an error
                        if state == 'ERROR':

                            # Sets the global status as not prestaged
                            prestaged = False

                            # Prints the error and attempts to prestage the file
                            print('ERROR: {0}'.format(stderr))
                            print('Attempting to prestage file {0}'.format(file_path))
                            cmd = subprocess.Popen([self.stager, 'prestage', file_path],
                                                  stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                            state, stderr = cmd.communicate()
                            state = state.strip()
                            stderr = stderr.strip()

                            # If the prestage request was not successful
                            if state == 'ERROR':
                                # Prints the error and attempts to prestage the file
                                print('ERROR: {0}'.format(stderr))

                        elif state in ['STAGEIN', 'STAGEOUT']:

                            # Sets the global status as not prestaged
                            prestaged = False

                    # If the run is not fully prestaged
                    if not prestaged:
                        # Returns the new job information
                        return {
                                'id':run_id,
                                'type':job_type,
                                'state':'PRE-STAGE'
                                }

                # Define the path of the JSON submit file
                run_path = '{0}/{1}'.format(self.json_path, run_id)
                filename = '{0}/{1}.json'.format(run_path, str(time.time()))

                # If the path of the JSON files doesn't exists
                if not os.path.exists(run_path):
                    # Create paths
                    print('Creating run submit files folder: {0}'.format(run_path))
                    os.makedirs(run_path)

                print('Generating JSON submit file: {0}'.format(filename))

                file_dao = FileDAO()

                # Create a dictionary with a list of files to submit
                files_dict = { 'Files' : [] }

                # Check if there're parameters for the servers
                source_url_params = '?{0}'.format(self.source_srv_params) if len(self.source_srv_params) > 0 else ''
                destination_url_params = '?{0}'.format(self.destination_srv_params) if len(self.destination_srv_params) > 0 else ''

                # If the job type is archive
                if job_type == 'archive':

                    # Gets a list of non archived files
                    files = file_dao.get_not_archived(run_id)

                    # Adds the non archived files to the files dictionary
                    for each_file in files:
                        files_dict['Files'].append({
                            'sources' : [self.source_server + self.source_prefix + each_file.path + source_url_params],
                            'destinations' : [self.destination_server + self.destination_prefix + each_file.path + destination_url_params],
                            'metadata' : 'file-metadata',
                            'checksums' : 'ADLER32:' + each_file.adler32,
                            'filesize' : each_file.filesize,
                            'activity' : 'Production',
                            'selection_strategy' : 'orderly'
                        })

                # If the job type is retrieve
                elif job_type == 'retrieve':

                    # Gets a list of non staged files
                    files = file_dao.get_not_staged(run_id)

                    # Adds the non staged files to the files dictionary
                    for each_file in files:
                        files_dict['Files'].append({
                            'sources' : [self.destination_server + self.destination_prefix + each_file.path + destination_url_params],
                            'destinations' : [self.source_server + self.source_prefix + each_file.path + source_url_params],
                            'metadata' : 'file-metadata',
                            'checksums' : 'ADLER32:' + each_file.adler32,
                            'filesize' : each_file.filesize,
                            'activity' : 'Production',
                            'selection_strategy' : 'orderly'
                        })

                # Create the JSON file
                with open(filename, 'w') as ofile:
                    json.dump(files_dict, ofile, indent=4)

                # Submit the new transfer job
                print('Attempting to submit the new job to FTS3')
                cmd = subprocess.Popen([self.fts3, 'submit_job', filename],
                                        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                job_id, stderr = cmd.communicate()

                # If there're no errors
                if len(stderr) == 0:

                    # Creates a new Job instance for the new transfer
                    job_id = job_id.strip()

                    job = Job()
                    job.job_id = job_id
                    job.job_type = job_type
                    job.start_date = datetime.now()
                    job.run_id = run_id
                    job.submit_file = filename

                    job_dao.insert_job(job)

                    print('Submitted job {0}'.format(job_id))

                    # Returns the new job information
                    return {
                            'id':run_id,
                            'type':job_type,
                            'job_id':job.job_id,
                            'state':job.job_state
                           }

                else:
                    print('Error generating the JSON submit file. {0}'.format(stderr))

            else:

                state = ''
                if job_type == 'archive':
                    state = 'archived'
                elif job_type == 'retrieve':
                    state = 'staged'

                print('The run {0} is already {1}'.format(run_id, state))

        else:
            print('The run {0} does not exist'.format(run_id))
