# Streamers to tape

This application is a tool to transfer files between two filesystems, **EOS** and **CASTOR**, using **FTS3**.

The design of this development is meant to easily change the filesystems or transfer tools in case that is needed. Of course, these changes requires certain development effort.

## Project structure

The project has two major folders destined to bash scripts and python modules. Currently, the only bash scripts corresponds to the **FTS3 wrapper**.

```bash
├── bash # Bash scripts 
└── python # Python modules
    ├── controllers # Controller modules
    ├── dao # Data Access Objects. Direct interaction with the database
    ├── models # Model modules
    └── utils # Utilities to interact with the database, configuration files, etc
```

## Deployment
The deployment of this application has 3 major steps:
1. Install dependencies.
2. Configure application.

This installation assumes all tools installed on the machine, like **FTS3 command line tool**.

### Install dependencies:
This application is meant to run on **lxplus** using **Python 2.6** and **virtualenv**. Taking that in mind, create a python virtual environment is required (and recommended). Once the **virtualenv** was created, clone the application repository in it:

```bash
$ virtualenv streamer-to-tape-venv
$ cd streamer-to-tape-venv/
$ ssh-add ~/.ssh/ssh-keys/cern_gitlab
$ git clone ssh://git@gitlab.cern.ch:7999/cms-tier0-ops/streamer-to-tape.git
```

After that, upgrade `pip` on the **virtualenv** and install the required python dependencies:

```bash
$ source bin/activate
(streamer-to-tape-venv)$ pip install pip==9.0.3
(streamer-to-tape-venv)$ cd streamer-to-tape/python
(streamer-to-tape-venv)$ pip install -r requirements.txt
(streamer-to-tape-venv)$ deactivate
```
**Note:** pip 9.0.3 is the last version that supports Python 2.6.

### Configure application:

There's a configuration file used to define the runtime values of the application. The objective of this file is to centralize the most dynamic values on a single place in order to avoid recoding if it's not necessary.

The configuration file is called *config* and it must be placed in the same folder where the application is executed.

**Note:** The configuration file is loaded by the Python module `python/utils/config.py`.

The configuration has the following sections:

#### Source: Variables related to the source server
- **name:** Alias for the source server.
- **server:** Server name.
- **path:** Path where the streamer files are stored on the server. This part of the folder tree will not be replicated on the destination folder.

#### Destination: Variables related to the destination server
- **name:** Alias for the destination server.
- **server:** Server name.
- **path:** Path where the streamer files are stored on the server. This part of the folder tree could be different respect to the source server.

#### Paths: Paths used by the application
- **data:** Data path where the streamer files are stored. This path is the same on the source and destination server.
- **workdir:** Working directory.
- **json:** Path where the JSON submit files are stored.
- **logs:** Path where the logs are stored.

#### Files: Special files used for the application
- **runs:** JSON file with the runs to archive/retrieve/purge from/to tape.
- **database:** Database file used for the application to store runs related information.
- **voms_proxy:** Proxy used by FTS3 to submit the transfer jobs.

#### Scripts: Wrapper scripts used for the application
- **fts3:** FTS3 wrapper script to abstract the FTS3 functionalities.

### Paths:
The paths for the streamers are constructed using the server name, a prefix folder and a data folder. The prefix allows the streamers data folder to be stored in different places in the source and destination folders, but replicating the relevant folder structure to make all the transfers easier.

The paths are contructed as follows `server_name/server_path/data_path` where:

- **server_name:** Source or destination server.
- **server_path:** Source or destination path.
- **data_path:** Data path (Paths section).

#### Example:
```ini
[Source]
server = gsiftp://eoscmsftp.cern.ch
path = /eos/cms/store/t0streamer

[Destination]
server = srm://srm-cms.cern.ch
path = /castor/cern.ch/cms/store/t0streamer

[Paths]
data = Data
```

Taking the configuration above, the paths would be:

- **Source** -> gsiftp://eoscmsftp.cern.ch/eos/cms/store/t0streamer/Data

- **Destination** -> srm://srm-cms.cern.ch/castor/cern.ch/cms/store/t0streamer/Data

**Note:** The data path is shared between source and destination servers, everything else could be different.

In some cases, the server requires additional parameters. To append additional parameters to the server URL, just add the list of parameters at the end of the `server` variable in configuration file preceded by `?` symbol. For example, in the case that xrootd protocol is used, it's necessary to append the `svcClass` parameter:

```ini
[Source]
server = root://eoscms.cern.ch?svcClass=t0cms
path = //eos/cms/store/t0streamer

[Destination]
server = root://castorcms.cern.ch?svcClass=t0cms
path = //castor/cern.ch/cms/store/t0streamer

[Paths]
data = Data
```

**Note:** The paths in xrootd protocol must be absolute. This is specified via `//` at the beginning of the path.

### Runs JSON file:

The runs file is used to submit transfer jobs to **FTS3**. The file consist on a list of runs that contains a job type (archive or retrieve) and a run id:

```json
{
    "runs": [
        {
            "type": "archive",
            "id": "300811"
        }
    ]
}
```

Once a job is submitted, it is updated each time that the software runs.

```json
{
    "runs": [
        {
            "state": "FINISHED",
            "type": "archive",
            "id": "300811",
            "job_id": "ca23e224-459f-11e8-aea3-02163e018830"
        }
    ]
}
```

**Note:** The FTS3 jobs are deleted from the system each couple of weeks, so the *job_id* may not be accessible after relatively small windows of time. That's one of the reasons why the related information is kept on a database.

## Execute application
The project includes a run script `run.sh` that set up the project location, creates a lock file (to avoid the execution of multiple instances of the application) and an output log.
After setup the variables included on this script, it just need only to be exectued, manually or from as a cronjob:

```bash
# Job to execute streamer-to-tape application
0 * * * * lxplus.cern.ch /afs/cern.ch/user/u/user/private/python_venvs/streamer-to-tape/src/python/run.sh 
```